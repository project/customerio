CUSTOMER.IO INTEGRATION MODULE
------------------------------

CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Usage
 * Troubleshooting
 * Maintainers

INTRODUCTION
------------

This module provides integration with Customer.io via its Track API, allowing
for the automatic publishing of user data from your Drupal site to Customer.io.
It is useful for synchronizing user information and activities to Customer.io
for further analysis and personalized user engagement.

REQUIREMENTS
------------

This module requires the following modules:
 * None

This module requires the following libraries:
 * printu/customerio

Additionally, a valid Customer.io account and API credentials (API Key and Site ID)
are required.

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules
   for further information.

CONFIGURATION
-------------

1. Navigate to Administration > Extend and enable the module.
2. Go to Administration > Configuration > System > Customer.io Integration
   (admin/config/services/customerio).
3. Enter your Customer.io API Key and Site ID.
4. Save the configuration.

USAGE
-----

Once configured, the module automatically tracks user events such as creation,
update, and deletion, and sends this data to Customer.io. The module dispatches
events that other modules can subscribe to for extending functionalities.

TROUBLESHOOTING
---------------

If you encounter issues with data not being sent to Customer.io, ensure that:

 * The API Key and Site ID are correctly configured.
 * Your Customer.io account is active and properly set up to receive data.
 * There are no connectivity issues between your Drupal site and Customer.io.

For more detailed troubleshooting, enable logging and check the logs for any
error messages.
