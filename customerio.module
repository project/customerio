<?php

use Drupal\Core\Form\FormStateInterface;
use Drupal\customerio\Event\UserCreatedEvent;
use Drupal\customerio\Event\UserDeletedEvent;
use Drupal\customerio\Event\UserUpdatedEvent;
use Drupal\user\UserInterface;

/**
 * Implements hook_form_alter().
 */
function customerio_form_field_config_edit_form_alter(&$form, FormStateInterface $form_state) {
  if (isset($form['#entity']) && $form['#entity'] instanceof UserInterface) {
    $field_config = $form_state->getFormObject()->getEntity();
    $enabled = $field_config->getThirdPartySetting('customerio', 'enabled', FALSE);

    $form['customerio_track_api'] = [
      '#type' => 'checkbox',
      '#title' => t('Enable Customer.io integration'),
      '#default_value' => $enabled,
      '#description' => t('Check this box to enable publishing of this field to Customer.io.'),
    ];

    // Add submit handler to save the third-party setting
    $form['actions']['submit']['#submit'][] = 'customerio_field_config_form_submit';
  }
}

/**
 * Custom submit handler for field config forms.
 */
function customerio_field_config_form_submit($form, FormStateInterface $form_state) {
  $field_config = $form_state->getFormObject()->getEntity();
  $enabled = $form_state->getValue('customerio_track_api');
  $field_config->setThirdPartySetting('customerio', 'enabled', $enabled)->save();
}

/**
 * Implements hook_user_insert().
 *
 * Handles the creation of a new user.
 * Dispatches a UserCreatedEvent when a new user is created, facilitating
 *  integration with Customer.io.
 *
 * @param UserInterface $user
 * @return void
 */
function customerio_user_insert(UserInterface $user): void {
  $event = new UserCreatedEvent($user);
  \Drupal::service('event_dispatcher')->dispatch($event, UserCreatedEvent::EVENT_NAME);
}

/**
 * Implements hook_user_update().
 *
 *  Handles the update of a new user.
 *  Dispatches a UserUpdatedEvent when a user is updated, facilitating
 *   integration with Customer.io.
 *
 * @param UserInterface $user
 * @return void
 */
function customerio_user_update(UserInterface $user): void {
  $event = new UserUpdatedEvent($user);
  \Drupal::service('event_dispatcher')->dispatch($event, UserUpdatedEvent::EVENT_NAME);
}

/**
 * Implements hook_user_delete().
 *
 *  Handles the deletion of a new user.
 *  Dispatches a UserDeletedEvent when a user is deleted, facilitating
 *   integration with Customer.io.
 *
 * @param UserInterface $user
 * @return void
 */
function customerio_user_delete(UserInterface $user): void {
  $event = new UserDeletedEvent($user);
  \Drupal::service('event_dispatcher')->dispatch($event, UserDeletedEvent::EVENT_NAME);
}
