<?php

namespace Drupal\customerio\EventSubscriber;

use Customerio\Client;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\customerio\Event\UserEventInterface;
use Drupal\customerio\Service\TrackApiDataService;
use Drupal\user\RoleInterface;
use Drupal\user\UserInterface;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\customerio\Event\UserCreatedEvent;
use Drupal\customerio\Event\UserUpdatedEvent;
use Drupal\customerio\Event\UserDeletedEvent;
use Drupal\user\Entity\Role;

/**
 * Event subscriber for Customer.io related user events.
 *
 * This class subscribes to various user-related events to handle integration
 * with Customer.io. It utilizes the Track API Data Service to send data
 * to Customer.io when users are created, updated, or deleted in the Drupal
 * system. This allows for automated synchronization of user data with
 * Customer.io for various purposes like analytics, marketing, and more.
 */
class CustomerioEventSubscriber implements EventSubscriberInterface {
  use LoggerChannelTrait;

  /**
   * The Track API Data Service.
   *
   * This service is responsible for sending data to Customer.io's Track API.
   * It abstracts the communication between Drupal and Customer.io, ensuring
   * data is formatted and sent correctly.
   *
   * @var \Drupal\customerio\Service\TrackApiDataService
   */
  protected TrackApiDataService $api;

  /**
   * Constructs a CustomerioEventSubscriber object.
   *
   * @param \Drupal\customerio\Service\TrackApiDataService $apiDataService
   *   The Track API data service to use for communicating with Customer.io.
   */
  public function __construct(TrackApiDataService $apiDataService) {
    $this->api = $apiDataService;
  }

  /**
   * Returns an array of event names this subscriber wants to listen to.
   *
   * Each key in the array is an event name and the value is the method that
   * should be called in this class whenever the respective event is fired.
   *
   * @return string[]
   *   An array of event names and their corresponding methods in this class.
   */
  public static function getSubscribedEvents(): array {
    return [
      UserCreatedEvent::EVENT_NAME => 'onUserCreated',
      UserUpdatedEvent::EVENT_NAME => 'onUserUpdated',
      UserDeletedEvent::EVENT_NAME => 'onUserDeleted',
    ];
  }

  /**
   * Handles the user creation event.
   *
   * This method is triggered when a new user is created in Drupal. It uses
   * the Track API Data Service to add the user's information to Customer.io.
   * This can include user attributes and other relevant data, which
   * can then be used in Customer.io for marketing campaigns, analytics, etc.
   *
   * @param \Drupal\customerio\Event\UserCreatedEvent $event
   *   The event object containing information about the created user.
   */
  public function onUserCreated(UserCreatedEvent $event): void {
    $this->api->add($event);
  }

  /**
   * Handles the user update event.
   *
   * Called when a user's information is updated. It ensures that the changes
   * made to the user's profile are reflected in Customer.io. This synchronization
   * helps maintain up-to-date user data for accurate targeting and analytics.
   *
   * @param \Drupal\customerio\Event\UserUpdatedEvent $event
   *   The event object containing information about the updated user.
   */
  public function onUserUpdated(UserUpdatedEvent $event): void {
    $this->api->update($event);
  }

  /**
   * Handles the user deletion event.
   *
   * This method responds to the deletion of a user in Drupal. It communicates
   * with Customer.io to remove the user's data, keeping the Customer.io database
   * in sync with Drupal's user base. This action is crucial for data accuracy
   * and respecting user privacy.
   *
   * @param \Drupal\customerio\Event\UserDeletedEvent $event
   *   The event object containing information about the deleted user.
   */
  public function onUserDeleted(UserDeletedEvent $event): void {
    $this->api->delete($event);
  }
}
