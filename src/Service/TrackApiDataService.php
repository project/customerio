<?php

namespace Drupal\customerio\Service;

use Customerio\Client;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\customerio\Event\UserCreatedEvent;
use Drupal\customerio\Event\UserEventInterface;
use Drupal\customerio\Event\UserUpdatedEvent;
use Drupal\field\Entity\FieldConfig;
use Drupal\user\Entity\Role;
use Drupal\user\RoleInterface;
use Drupal\user\UserInterface;
use GuzzleHttp\Exception\GuzzleException;

/**
 * Service to interact with Track Api.
 */
class TrackApiDataService {
  use LoggerChannelTrait;

  /**
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected ConfigFactoryInterface $configFactory;

  /**
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   */
  public function __construct(ConfigFactoryInterface $configFactory) {
    $this->configFactory = $configFactory;
  }

  /**
   * @param \Drupal\customerio\Event\UserEventInterface $event
   *
   * @return void
   */
  public function add(UserEventInterface $event): void {
    $client = $this->initializeClient();
    try {
      $client->customers()->add($this->getUserData($event));
    } catch (\Exception|GuzzleException $e) {
      $this->logError($event, $e->getMessage());
    }
  }

  /**
   * @param \Drupal\customerio\Event\UserEventInterface $event
   *
   * @return void
   */
  public function update(UserEventInterface $event): void {
    $client = $this->initializeClient();
    try {
      $client->customers()->add($this->getUserData($event));
    } catch (\Exception|GuzzleException $e) {
      $this->logError($event, $e->getMessage());
    }
  }

  /**
   * @param \Drupal\customerio\Event\UserEventInterface $event
   *
   * @return void
   */
  public function delete(UserEventInterface $event): void {
    $client = $this->initializeClient();
    try {
      $client->customers()->delete($this->getUserData($event));
    } catch (\Exception|GuzzleException $e) {
      $this->logError($event, $e->getMessage());
    }
  }

  /**
   * @param \Drupal\customerio\Event\UserEventInterface $event
   * @param string $exception_message
   *
   * @return void
   */
  protected function logError(UserEventInterface $event, string $exception_message): void {
    $message = sprintf(
      "Error while adding customer data for event '%s' with user ID '%s': %s",
      get_class($event),
      $event->getUser()->id(),
      $exception_message
    );
    $this->getLogger('customerio')->error($message);
  }

  /**
   * @return \Customerio\Client|null
   */
  private function initializeClient(): ?Client {
    $config = $this->configFactory->get('customerio.settings');
    $apiKey = $config->get('api_key');
    $apiSiteId = $config->get('api_siteid');

    if (!empty($apiKey) && !empty($apiSiteId)) {
      // Initialize the Customer.io client with API key and siteId.
      try {
        return new Client($apiKey, $apiSiteId);
      }
      catch (\Exception $exception) {
        $this->getLogger('customerio')->error('Error initializing Customer.io client: @message', ['@message' => $exception->getMessage()]);
      }
    }
    else {
      $this->getLogger('customerio')->warning('Customer.io API Key or Site ID is not configured.');
    }

    return null;
  }

  /**
   * Helper function to get user role names.
   */
  protected function getUserRoleNames(UserInterface $user): array {
    $role_ids = $user->getRoles();
    $role_names = [];

    foreach ($role_ids as $role_id) {
      $role = Role::load($role_id);
      if ($role instanceof RoleInterface) {
        $role_names[] = $role->label();
      }
    }

    return $role_names;
  }

  /**
   * Prepares and retrieves user data.
   *
   * @param \Drupal\customerio\Event\UserEventInterface $event
   *
   * @return array
   *   The array of user data.
   */
  protected function getUserData(UserEventInterface $event): array {
    $user = $event->getUser();
    $user_data = [
      'id' => $user->id(),
      'email' => $user->getEmail()
    ];

    if ($event instanceof UserCreatedEvent or $event instanceof UserUpdatedEvent) {
      $role_names = $this->getUserRoleNames($user);
      $user_data['roles'] = $role_names;
    }

    $enabled_user_fields = $this->getEnabledUserFields($user);
    if (!empty($enabled_user_fields)) {
      $user_data = $user_data + $enabled_user_fields;
    }

    // Allow other modules to alter the user data.
    \Drupal::moduleHandler()->alter('customerio_user_data', $user_data, $user, $event);

    return $user_data;
  }

  protected function getEnabledUserFields($user) {
    $enabledUserFieldsInfo = [];

    $fields = FieldConfig::loadMultiple();
    foreach ($fields as $field) {
      // Check if the field is associated with the user entity
      if ($field->getTargetEntityTypeId() == 'user' && $field->getThirdPartySetting('customerio', 'enabled', FALSE)) {
        $value = $this->getFieldValue($user, $field->getName());
        if ($value !== NULL) {
          $enabledUserFieldsInfo[$field->getName()] = $value;
        }
      }
    }

    return $enabledUserFieldsInfo;
  }

  private function getFieldValue($user, $field_name) {
    $field = $user->get($field_name);

    if (!$field || $field->isEmpty()) {
      return NULL;
    }

    $field_type = $field->getFieldDefinition()->getType();

    switch ($field_type) {
      case 'string':
      case 'text':
      case 'text_long':
      case 'text_with_summary':
        // Return the first value for text fields
        return $field->value;

      case 'entity_reference':
        // For entity references, you might want to return target entity IDs or labels
        $references = [];
        foreach ($field->referencedEntities() as $entity) {
          $references[] = $entity->label(); // or $entity->id() if you need IDs
        }
        return implode(', ', $references); // Join multiple references as a string

      case 'integer':
      case 'float':
      case 'decimal':
        // Return the value for numeric fields
        return $field->value;

      default:
        // Handle other field types or unknown field types
        return 'Field type not supported';
    }
  }
}
