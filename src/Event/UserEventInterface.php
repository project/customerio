<?php

namespace Drupal\customerio\Event;

use Drupal\user\UserInterface;

/**
 * Provides an interface for user-related events.
 *
 * This interface is used as a base for events that are related to user
 * entities in the system. It defines a standard method to retrieve the user
 * entity involved in the event, ensuring consistency across different types
 * of user-related events, such as creation, deletion, or update.
 *
 * Implementing this interface allows different parts of the system, including
 * modules and external integrations, to handle user-related events in a
 * standardized way.
 */
interface UserEventInterface {

  /**
   * Gets the user entity.
   *
   * @return \Drupal\user\UserInterface
   *   The user entity being manipulated.
   */
  public function getUser(): UserInterface;
}
