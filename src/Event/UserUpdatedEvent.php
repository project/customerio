<?php

namespace Drupal\customerio\Event;

use Drupal\Component\EventDispatcher\Event;
use Drupal\user\UserInterface;

/**
 * Defines the User Updated Event.
 *
 * This event is dispatched when a user is created from the system.
 * It encapsulates information about the created user and is useful for
 * modules that need to react to user update, such as external integrations
 * like Customer.io.
 *
 * @see \Drupal\customerio\Event\UserEventInterface
 */
class UserUpdatedEvent extends Event implements UserEventInterface {
  /**
   * The name of the event.
   */
  const EVENT_NAME = 'customerio.user_updated';

  /**
   * The user entity being updated.
   *
   * @var \Drupal\user\UserInterface
   */
  protected UserInterface $user;

  /**
   * Constructs a new UserUpdatedEvent.
   *
   * @param \Drupal\user\UserInterface $user
   *   The user entity being updated.
   */
  public function __construct(UserInterface $user) {
    $this->user = $user;
  }

  /**
   * Gets the user entity.
   *
   * @return \Drupal\user\UserInterface
   *   The user entity being updated.
   */
  public function getUser(): UserInterface {
    return $this->user;
  }
}
