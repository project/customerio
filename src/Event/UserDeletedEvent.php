<?php

namespace Drupal\customerio\Event;

use Drupal\Component\EventDispatcher\Event;
use Drupal\user\UserInterface;

/**
 * Defines the User Deleted Event.
 *
 * This event is dispatched when a user is deleted from the system.
 * It encapsulates information about the deleted user and is useful for
 * modules that need to react to user deletion, such as external integrations
 * like Customer.io.
 *
 * @see \Drupal\customerio\Event\UserEventInterface
 */
class UserDeletedEvent extends Event implements UserEventInterface {
  /**
   * The name of the event.
   */
  const EVENT_NAME = 'customerio.user_deleted';

  /**
   * The user entity being deleted.
   *
   * @var \Drupal\user\UserInterface
   */
  protected UserInterface $user;

  /**
   * Constructs a new UserDeletedEvent.
   *
   * @param \Drupal\user\UserInterface $user
   *   The user entity being deleted.
   */
  public function __construct(UserInterface $user) {
    $this->user = $user;
  }

  /**
   * Gets the user entity.
   *
   * @return \Drupal\user\UserInterface
   *   The user entity being deleted.
   */
  public function getUser(): UserInterface {
    return $this->user;
  }
}
