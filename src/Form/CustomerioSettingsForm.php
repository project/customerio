<?php

namespace Drupal\customerio\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

class CustomerioSettingsForm extends ConfigFormBase {

  protected function getEditableConfigNames() {
    return [
      'customerio.settings',
    ];
  }

  public function getFormId() {
    return 'customerio_admin_settings';
  }

  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('customerio.settings');

    $form['api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Key'),
      '#default_value' => $config->get('api_key'),
      '#required' => TRUE,
    ];

    $form['api_siteid'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Site Id'),
      '#default_value' => $config->get('api_siteid'),
      '#required' => TRUE,
    ];

    $form['transactional_app_api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('App API Key'),
      '#default_value' => $config->get('transactional_app_api_key'),
      '#required' => FALSE,
    ];

    return parent::buildForm($form, $form_state);
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('customerio.settings')
        ->set('api_key', $form_state->getValue('api_key'))
        ->set('api_siteid', $form_state->getValue('api_siteid'))
        ->set('transactional_app_api_key', $form_state->getValue('transactional_app_api_key'))
        ->save();

    parent::submitForm($form, $form_state);
  }
}
