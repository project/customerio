<?php

namespace Drupal\customerio_transactional\Transport;

use Drupal\customerio_transactional\Service\TransactionalApiService;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Mailer\Exception\UnsupportedSchemeException;
use Symfony\Component\Mailer\Transport\AbstractTransportFactory;
use Symfony\Component\Mailer\Transport\Dsn;
use Symfony\Component\Mailer\Transport\TransportInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

/**
 * Factory class for creating instances of TransactionalApiTransport.
 *
 * This factory extends AbstractTransportFactory and is responsible for creating
 * transport instances specifically for Customer.io transactional email API.
 */
class TransactionalApiTransportFactory extends AbstractTransportFactory {

  /**
   * The API service for handling transactions.
   *
   * @var TransactionalApiService
   */
  protected TransactionalApiService $apiService;

  /**
   * Constructs a TransactionalApiTransportFactory object.
   *
   * @param TransactionalApiService $transactional_api_service
   *   The API service used for handling Customer.io transactions.
   * @param EventDispatcherInterface|null $dispatcher
   *   The event dispatcher service (optional).
   * @param HttpClientInterface|null $client
   *   The HTTP client service (optional).
   * @param LoggerInterface|null $logger
   *   The logger service (optional).
   */
  public function __construct(TransactionalApiService $transactional_api_service, ?EventDispatcherInterface $dispatcher = null, ?HttpClientInterface $client = null, ?LoggerInterface $logger = null) {
    $this->apiService = $transactional_api_service;
    parent::__construct($client, $dispatcher, $logger);
  }

  /**
   * Creates a transport instance for the given DSN.
   *
   * This method overrides the abstract method in AbstractTransportFactory. It
   * checks if the provided DSN is for 'customerio' and creates a new instance
   * of TransactionalApiTransport with the necessary services.
   *
   * @param Dsn $dsn
   *   The DSN used to create the transport.
   * @return TransportInterface
   *   The created transport instance.
   * @throws UnsupportedSchemeException
   *   Thrown if the DSN scheme is not supported by this factory.
   */
  public function create(Dsn $dsn): TransportInterface {
    if ('customerio' === $dsn->getScheme()) {
      $host = 'default' === $dsn->getHost() ? null : $dsn->getHost();
      return (new TransactionalApiTransport($this->apiService, $this->dispatcher, $this->logger))->setHost($host);
    }

    throw new UnsupportedSchemeException($dsn, 'customerio', $this->getSupportedSchemes());
  }


  /**
   * Returns an array of supported schemes for this transport factory.
   *
   * @return array
   *   An array of supported schemes ('customerio' in this case).
   */
  protected function getSupportedSchemes(): array {
    return ['customerio'];
  }

}
