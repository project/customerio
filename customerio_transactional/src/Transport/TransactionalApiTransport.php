<?php

namespace Drupal\customerio_transactional\Transport;

use Drupal\customerio_transactional\Service\TransactionalApiService;
use Psr\EventDispatcher\EventDispatcherInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Mailer\Envelope;
use Symfony\Component\Mailer\SentMessage;
use Symfony\Component\Mailer\Transport\AbstractApiTransport;
use Symfony\Component\Mime\Email;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

/**
 * Defines the Customer.io API transport mechanism for transactional emails.
 *
 * This class extends the AbstractApiTransport to implement sending of emails
 * via the Customer.io Transactional API. It integrates with the TransactionalApiService
 * to handle specific API interactions.
 */
class TransactionalApiTransport extends AbstractApiTransport {
  // Default host for the Customer.io API.
  private const HOST = 'api.customer.io';

  /**
   * The API service for handling transactions.
   *
   * @var TransactionalApiService
   */
  private TransactionalApiService $apiService;

  /**
   * Constructs a TransactionalApiTransport object.
   *
   * @param TransactionalApiService $api_service
   *   The API service for handling Customer.io transactions.
   * @param HttpClientInterface|null $client
   *   The HTTP client service (optional).
   * @param EventDispatcherInterface|null $dispatcher
   *   The event dispatcher service (optional).
   * @param LoggerInterface|null $logger
   *   The logger service (optional).
   */
  public function __construct(TransactionalApiService $api_service, ?HttpClientInterface $client = null, ?EventDispatcherInterface $dispatcher = null, ?LoggerInterface $logger = null) {
    $this->apiService = $api_service;

    parent::__construct($client, $dispatcher, $logger);
  }

  /**
   * Converts the transport instance to its string representation.
   *
   * @return string
   *   The string representation of the transport instance.
   */
  public function __toString(): string {
    return sprintf('customerio://%s', $this->getEndpoint());
  }

  /**
   * Sends a transactional email via the Customer.io API.
   *
   * Delegates the email sending process to the TransactionalApiService, passing
   * along necessary parameters like the endpoint, email details, and envelope.
   *
   * @param SentMessage $sentMessage
   *   The sent message instance.
   * @param Email $email
   *   The email to be sent.
   * @param Envelope $envelope
   *   The envelope containing additional sending details.
   *
   * @return ResponseInterface
   *   The response from the Customer.io API.
   */
  protected function doSendApi(SentMessage $sentMessage, Email $email, Envelope $envelope): ResponseInterface {
    return $this->apiService->sendTransactionalEmail($this->getEndpoint(), $email, $envelope);
  }

  /**
   * Retrieves the API endpoint.
   *
   * Returns the endpoint for the API requests. If not set, defaults to the class constant HOST.
   *
   * @return string|null
   *   The API endpoint or null if not set.
   */
  private function getEndpoint(): ?string {
    return ($this->host ?: self::HOST);
  }
}
