<?php

namespace Drupal\customerio_transactional\Plugin\MailerTransport;

use Drupal\Core\Form\FormStateInterface;
use Drupal\customerio_transactional\Transport\TransactionalApiTransportFactory;
use Drupal\symfony_mailer\Plugin\MailerTransport\TransportBase;
use Symfony\Component\Mailer\Transport;

/**
 * Defines the Customer.io Transactional API transport plugin.
 *
 * This plugin provides a mailer transport method for Drupal, enabling the
 * system to send transactional emails and messages using Customer.io's API.
 *
 * @MailerTransport(
 *   id = "customerio_transactional",
 *   label = @Translation("Customer.io Transactional API Transport"),
 *   description = @Translation("Enables sending of transactional emails and messages through Customer.io's API."),
 * )
 */
class TransactionalApiMailerTransport extends TransportBase {
  /**
   * @inheritDoc
   */
  public function defaultConfiguration(): array {
    return [
      'dsn' => '',
    ];
  }

  /**
   * @inheritDoc
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $form['dsn'] = [
      '#type' => 'textfield',
      '#title' => $this->t('DSN'),
      '#maxlength' => 255,
      '#default_value' => $this->configuration['dsn'],
      '#description' => $this->t('DSN for the Transport'),
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * @inheritDoc
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state): void {
    $dsn_string = $form_state->getValue('dsn');
    try {
      /** @var \Drupal\symfony_mailer\TransportFactoryManager $transport_manager */
      $transport_manager = \Drupal::service('symfony_mailer.transport_factory_manager');
      $factories = $transport_manager->getFactories();
      $isValid = FALSE;
      $dsn = Transport\Dsn::fromString($dsn_string);
      foreach ($factories as $factory) {
        if ($factory instanceof TransactionalApiTransportFactory && $factory->supports($dsn)) {
          $isValid = TRUE;
          break;
        }
      }
      if (!$isValid) {
        throw new \Exception('Invalid transport');
      }
    }
    catch (\Exception $e) {
      $form_state->setErrorByName('dsn', $this->t('Invalid DSN: @message', ['@message' => $e->getMessage()]));
    }
  }

  /**
   * @inheritDoc
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state): void {
    $this->configuration['dsn'] = $form_state->getValue('dsn');
  }

  /**
   * @inheritDoc
   */
  public function getDsn() {
    return $this->configuration['dsn'];
  }

}
