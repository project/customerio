<?php

namespace Drupal\customerio_transactional\Plugin\EmailAdjuster;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\customerio_transactional\Service\TransactionalApiService;
use Drupal\symfony_mailer\EmailInterface;
use Drupal\symfony_mailer\Processor\EmailAdjusterBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Placeholders plugin
 *
 * @EmailAdjuster(
 *   id = "cio_transactional_email_token",
 *   label = @Translation("Customer.io Transactional email placeholders"),
 *   description = @Translation("Sets email placeholders."),
 *   weight = 900,
 * )
 */
class TransactionalApiEmailTokensAdjuster extends EmailAdjusterBase implements ContainerFactoryPluginInterface {
  /**
   * The API service for handling transactions.
   *
   * @var TransactionalApiService
   */
  protected TransactionalApiService $apiService;

  /**
   * Constructs a new TransactionalApiEmailTokensAdjuster instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param TransactionalApiService $api_service
   *   The API service for handling transactions.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, TransactionalApiService $api_service) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->apiService = $api_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('customerio_transactional.api_service')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build(EmailInterface $email): void {
    $tokens = $this->configuration['tokens'];
    $headers = [];
    $token_service = \Drupal::token();
    $user = $email->getParam('user');

    $token_options = ['callback' => 'user_mail_tokens', 'clear' => TRUE];
    foreach ($tokens as $token){
      if (!empty($token['value'])) {
        $token_value = $token_service->replace($token['value'], ['user' => $user], $token_options);
        $headers[$token['name']] = $token_value;
      }
    }
    $email->addTextHeader('customer_io_transactional_template_tokens', json_encode($headers));
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state): array {
    $id = $this->getPluginId();
    $wrapper = "mail-tokens-edit-$id";
    $form['tokens'] = [
      '#type' => 'container',
      '#attributes' => ['id' => $wrapper]
    ];

    // Synchronise with any existing form state.
    $tokens = $form_state->getValue([
      'config',
      $id,
      'tokens'
    ]) ?? $this->configuration['tokens'] ?? [[]];

    foreach ($tokens as $item) {
      $form['tokens'][] = [
        '#type' => 'fieldset', // or 'container'
        '#attributes' => ['class' => ['container-inline']],
        'name' => [
          '#type' => 'textfield',
          '#title' => $this->t('Placeholder'),
          '#default_value' => $item['name'] ?? NULL,
        ],
        'value' => [
          '#type' => 'textfield',
          '#title' => $this->t('Value'),
          '#default_value' => $item['value'] ?? NULL,
          '#token_types' => ['user'],
          '#token_browser' => TRUE,
        ]
      ];
    }

    $form['tokens_wrapper'] = [
      '#type' => "container",
    ];

    $form['tokens_wrapper']['token_help'] = array(
      '#theme' => 'token_tree_link',
      '#token_types' => array('user'),
    );

    // Add Placeholder button.
    $form['add'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add Placeholder'),
      '#submit' => [[static::class, 'submitAdd']],
      '#ajax' => [
        'callback' => [static::class, 'ajaxUpdate'],
        'wrapper' => $wrapper,
      ],
    ];
    return $form;
  }

  /**
   * Ajax callback to update the form.
   */
  public static function ajaxUpdate($form, FormStateInterface $form_state) {
    $button = $form_state->getTriggeringElement();
    $id = $button['#parents'][1];
    return $form['config'][$id]['tokens'];
  }

  /**
   * Submit callback for add button.
   */
  public static function submitAdd(array &$form, FormStateInterface $form_state) {
    $button = $form_state->getTriggeringElement();
    $id = $button['#parents'][1];
    $tokens = $form_state->getValue(['config', $id, 'tokens']);
    $tokens[] = [];
    $form_state->setValue(['config', $id, 'tokens'], $tokens)
      ->setRebuild();
  }

  /**
   * {@inheritdoc}
   */
  public function getSummary() {
    if (!empty($this->configuration['value'])) {
      return $this->configuration['value'];
    }
    return NULL;
  }


}
