<?php

namespace Drupal\customerio_transactional\Plugin\EmailAdjuster;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\customerio_transactional\Service\TransactionalApiService;
use Drupal\symfony_mailer\EmailInterface;
use Drupal\symfony_mailer\Processor\EmailAdjusterBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines the Mailer transport Email Adjuster.
 *
 * @EmailAdjuster(
 *   id = "cio_transactional_email_template",
 *   label = @Translation("Customer.io Transactional email template"),
 *   description = @Translation("Sets the transactional mailer template."),
 *   weight = 900,
 * )
 */
class TransactionalApiEmailTemplateAdjuster extends EmailAdjusterBase implements ContainerFactoryPluginInterface {

  /**
   * The API service for handling transactions.
   *
   * @var TransactionalApiService
   */
  protected TransactionalApiService $apiService;

  /**
   * Constructs a new TransactionalApiEmailTemplateAdjuster instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param TransactionalApiService $api_service
   *   The API service for handling transactions.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, TransactionalApiService $api_service) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->apiService = $api_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('customerio_transactional.api_service')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build(EmailInterface $email): void {
    $email->addTextHeader('customer_io_transactional_template_id', $this->configuration['value']);
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state): array {
    $options = [];

    // Retrieve available API templates from the customer.io.
    $api_templates = $this->apiService->getAvailableTemplates();

    // Loop through the API templates to build the options array for the select element.
    foreach ($api_templates as $id => $api_template) {
      // The key of the options array is the template ID, and the value is the template name.
      $options[$id] = $api_template['name'];
    }

    $form['value'] = [
      '#type' => 'select',
      '#options' => $options,
      '#default_value' => $this->configuration['value'] ?? NULL,
      '#required' => TRUE,
      '#description' => $this->t('Customer.io mail template.'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getSummary() {
    if (!empty($this->configuration['value'])) {
      return $this->configuration['value'];
    }
    return NULL;
  }
}
