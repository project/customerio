<?php

namespace Drupal\customerio_transactional\Service;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Logger\LoggerChannelTrait;
use Symfony\Component\Mailer\Exception\TransportException;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Mailer\Envelope;

/**
 * Service class for handling interactions with the Customer.io Transactional API.
 */
class TransactionalApiService {
  use LoggerChannelTrait;

  /**
   * Configuration for 'customerio.settings'.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected ImmutableConfig $config;

  /**
   * HTTP client service for making API requests.
   *
   * @var HttpClientInterface
   */
  protected HttpClientInterface $client;

  /**
   * Config factory service to handle configuration.
   *
   * @var ConfigFactoryInterface
   */
  protected ConfigFactoryInterface $configFactory;

  /**
   * Constructs a new TransactionalApiService object.
   *
   * @param ConfigFactoryInterface $config_factory
   *   The configuration factory service.
   * @param HttpClientInterface $client
   *   The HTTP client service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, HttpClientInterface $client) {
    $this->config = $config_factory->get('customerio.settings');
    $this->client = $client;
    $this->configFactory = $config_factory;
  }

  /**
   * Retrieves available templates from the Customer.io Transactional API.
   *
   * This method fetches and returns a list of available email templates
   * from Customer.io, based on the configured API key. If the API key is
   * not configured, it returns NULL.
   *
   * @return array|null
   *   An array of available templates, or NULL if the API key is missing.
   */
  public function getAvailableTemplates(): ?array {
    // Retrieve the API key from the configuration.
    $app_api_key = $this->config->get('transactional_app_api_key');

    // Return NULL if the API key is not set or empty.
    if (empty($app_api_key)) {
      return NULL;
    }

    try {
      $response = $this->client->request('GET', 'https://api.customer.io/v1/transactional', [
        'headers' => [
          'Accept' => 'application/json',
          'Authorization' => 'Bearer ' . $app_api_key
        ]
      ]);

      $templates = json_decode($response->getContent(), TRUE);
      $options = array_column($templates['messages'], null, 'id');

      // Skip default template.
      $options = array_filter($options, function($item) {
        return $item['id'] !== 1;
      });

      return array_map(function($item) {
        return ['name' => $item['name'], 'description' => $item['description']];
      }, $options);
    }
    catch (ClientExceptionInterface|RedirectionExceptionInterface|ServerExceptionInterface|TransportExceptionInterface $exception) {
      // Log the exception details using the 'customerio_transactional' channel.
      $this->getLogger('customerio_transactional')->error('Exception occurred: @message', [
        '@message' => $exception->getMessage(),
      ]);
    }

    return NULL;
  }

  /**
   * Sends a transactional email via the specified API endpoint.
   *
   * This method handles the preparation and sending of a transactional email using
   * the Customer.io Transactional API. It includes error handling and logging
   * for both successful and unsuccessful attempts.
   *
   * @param string $endpoint
   *   The API endpoint to which the request will be sent.
   * @param Email $email
   *   The Email object containing the details of the email to be sent.
   * @param Envelope $envelope
   *   The Envelope object containing additional sending options.
   *
   * @return ResponseInterface
   *   The response from the API.
   */
  public function sendTransactionalEmail(string $endpoint, Email $email, Envelope $envelope) : ResponseInterface {
    $response = NULL;
    try {
      // Retrieve the API key from the configuration.
      $app_api_key = $this->config->get('transactional_app_api_key');

      // If the API key is empty, throw an exception to halt the process.
      if (empty($app_api_key)) {
        throw new \Exception('Failed to get app_api_key');
      }

      // Construct the request URL using the endpoint parameter.
      $request_url = 'https://' . $endpoint . '/v1/send/email';
      $response = $this->client->request('POST', $request_url, [
        'json' => $this->getPayload($email, $envelope),
        'auth_bearer' => $app_api_key,
      ]);

      // Extract the status code from the response.
      $status_code = $response->getStatusCode();
      if ($status_code == 200) {
        $this->getLogger('customerio_transactional')->info('Email sent successfully.');
      }
      else {
        $this->getLogger('customerio_transactional')->error('Failed to send email. Status code: @status_code, Endpoint: @endpoint', [
          '@status_code' => $status_code,
          '@endpoint' => $endpoint,
        ]);
      }
    } catch (\Exception|TransportExceptionInterface $exception) {
      // Log the exception details.
      $this->getLogger('customerio_transactional')->error('Exception occurred: @message. Trace: @trace', [
        '@message' => $exception->getMessage(),
        '@trace' => $exception->getTraceAsString(),
      ]);
    }

    return $response;
  }

  /**
   * Constructs the payload for a transactional email.
   *
   * This method builds an array payload to be sent to the API, using the
   * provided Email and Envelope objects. It includes essential details such as
   * the transactional message ID, recipient identifiers, and message data.
   *
   * @param $email
   *   The Email object containing the recipient and other email information.
   * @param $envelope
   *   The Envelope object containing additional sending options. Currently unused but can be
   *   utilized for future enhancements.
   *
   * @return array
   *   The constructed payload array for the API request.
   */
  private function getPayload(Email $email, Envelope $envelope): array {
    // Retrieve the transactional message ID.
    $transactional_message_id = $this->getTransactionalMessageId($email);
    $recipient_address = $email->getTo()[0]->getAddress();

    // Check if the recipient address is valid.
    if (empty($recipient_address)) {
      throw new \InvalidArgumentException("Recipient address is missing in the email object.");
    }
    $payload = [
      'transactional_message_id' => $transactional_message_id,
      'identifiers' => [
        'email' => $recipient_address,
      ],
      'to' => $recipient_address,
    ];

    // Message placeholders(tokens).
    $message_data = $this->getTransactionalMessageData($email);
    if (!empty($message_data)) {
      $payload['message_data'] = $message_data;
    }
    else {
      $this->getLogger('customerio_transactional')->warning('Empty message data for the email. To: @to, Transactional Message Id: @message_id', [
        '@to' => $recipient_address,
        '@message_id' => $transactional_message_id,
      ]);
    }

    return $payload;
  }

  /**
   * Retrieves the transactional message ID from an email's headers.
   *
   * This function extracts the Customer.io transactional template ID from the
   * headers of the provided Email object. It's used to identify the specific
   * transactional message template to be used in sending emails.
   *
   * @param Email $email
   *   The Email object from which to extract the transactional message ID.
   *
   * @return int
   *   The extracted transactional message ID, or 0 if not found.
   */
  private function getTransactionalMessageId(Email $email): int {
    $transactional_message_id = NULL;
    $header = $email->getHeaders()->get('customer_io_transactional_template_id');
    if (!empty($header)) {
      $transactional_message_id = trim($email->getHeaders()->get('customer_io_transactional_template_id')->getBodyAsString());
    }

    return (int) $transactional_message_id;
  }

  /**
   * Extracts and decodes the transactional message data from an email's headers.
   *
   * This function is designed to retrieve Customer.io template tokens from the
   * headers of the provided Email object. These tokens are used to personalize
   * the content of the transactional email.
   *
   * @param Email $email
   *   The Email object from which to extract the template tokens.
   *
   * @return array|null
   *   The decoded array of template tokens, or NULL if they are not found or on error.
   */
  private function getTransactionalMessageData(Email $email): ?array {
    try {
      $header = $email->getHeaders()->get('customer_io_transactional_template_tokens');
      if (!empty($header)) {
        return json_decode($header->getBodyAsString(), TRUE);
      }
    }
    catch (\Exception $exception) {
      $this->getLogger('customerio_transactional')->error('Failed to get template tokens from email.');
      return NULL;
    }

    return NULL;
  }
}
