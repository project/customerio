<?php

use Drupal\Core\Form\FormStateInterface;

/**
 * Implements hook_form_FORM_ID_alter().
 */
function customerio_transactional_form_mailer_policy_edit_form_alter(&$form, FormStateInterface $form_state, $form_id){
  // Add a custom validation.
  $form['#validate'][] = 'customerio_transactional_mailer_policy_form_validate';
}

/**
 * Validates the Customer.io transactional mailer policy form.
 *
 * This function checks whether the required configuration settings are provided
 * when using the Customer.io transactional email transport. It specifically ensures
 * that a transactional email template is selected if the Customer.io transactional
 * transport is chosen.
 *
 * @param array $form
 *   The form structure.
 * @param \Drupal\Core\Form\FormStateInterface $form_state
 *   The current state of the form.
 */
function customerio_transactional_mailer_policy_form_validate($form, FormStateInterface $form_state) {
  $config = $form_state->getValue('config');

  // Check if the email transport configuration is set and the submit button triggered the validation.
  $is_submit_trigger = $form_state->getTriggeringElement()['#value']->__toString() === $form["actions"]["submit"]['#value']->__toString();
  if (isset($config["email_transport"]) && $is_submit_trigger) {
    $email_transport = $config["email_transport"]['value'];

    // Validate specific settings for Customer.io transactional email transport.
    if ($email_transport === 'customerio_transactional' && !isset($config["cio_transactional_email_template"])) {
      // Set an error if the email template is not selected.
      $form_state->setErrorByName('email_transport', 'To use Customer.io transactional email transport, please select a transactional email template.');
    }
  }
}

